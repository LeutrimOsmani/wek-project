# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CSResource',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=40)),
                ('url', models.URLField()),
                ('description', models.CharField(max_length=400, blank=True)),
                ('grade_levels', models.CommaSeparatedIntegerField(max_length=28, null=True, blank=True)),
                ('rating_sum', models.IntegerField(default=0)),
                ('rating_count', models.IntegerField(default=0)),
                ('promotional_video_url', models.URLField(null=True, blank=True)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified_time', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'CS Resource',
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('name', models.CharField(default='OR', choices=[('C', 'C'), ('CS', 'C++'), ('JA', 'Java'), ('PN', 'Python'), ('CP', 'C#'), ('JT', 'JavaScript'), ('LA', 'Lua'), ('BY', 'Blockly (google)'), ('CY', 'Other visual blocky language'), ('TE', 'Turtle directions'), ('JA', 'Java'), ('CE', 'Clojure'), ('CT', 'CoffeeScript'), ('AH', 'Adobe Flash'), ('OR', 'Other')], max_length=2)),
                ('other_name', models.CharField(max_length=20, blank=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.CreateModel(
            name='Platform',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('name', models.CharField(default='OR', choices=[('AD', 'Android'), ('WS', 'Windows'), ('MC', 'Mac'), ('IE', 'IPhone'), ('ID', 'IPad'), ('WB', 'Web based'), ('UY', 'Unity'), ('LX', 'Linux'), ('CB', 'Chrome Book'), ('ME', 'Microsoft Surface'), ('JA', 'Java'), ('DC', 'Document(s)'), ('AR', 'Adobe AIR or Flash'), ('AH', 'Adobe Flash'), ('OR', 'Other')], max_length=2)),
                ('other_name', models.CharField(max_length=20, blank=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.CreateModel(
            name='PriceTier',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=40)),
                ('trial_days', models.IntegerField(null=True, blank=True)),
                ('cost', models.FloatField(null=True, blank=True)),
                ('unit', models.CharField(default='UT', choices=[('UT', 'Unit'), ('DY', 'Day'), ('MH', 'Month'), ('YR', 'Year'), ('UR', 'User'), ('SL', 'School'), ('TL', 'Trial')], max_length=2)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=400, blank=True)),
                ('rating', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified_time', models.DateTimeField(auto_now=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ScreenShot',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('image', models.ImageField(upload_to='')),
                ('description', models.CharField(max_length=40, null=True, blank=True)),
                ('cs_resource', models.ForeignKey(to='cslist.CSResource')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='review',
            unique_together=set([('user', 'cs_resource')]),
        ),
    ]
